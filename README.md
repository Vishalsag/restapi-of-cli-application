**REST API of Basic Inventory Management**  

**Prerequisite**  
+ Node
+ [Golang-migrate](https://github.com/golang-migrate/migrate/releases)

**Starting the server**  
This will install all dependencies required for the api.
```
npm install
```

This will run the server with nodemon.
```
npm start
```

**Migrating the dataset to PostgreSql database**  
+ The 2 dataset files are needed to migrate to the database.  

Here we use [golang-migrate](https://github.com/golang-migrate/migrate/releases) to migrate the files.
Install the migrate from the given link the tar.gz file into your system or from the packages folder.

1. To create database
```
psql -h localhost -U postgres -w -c "create database example;"
```

2. Export the database url to variable
```
export POSTGRESQL_URL='postgres://postgres:password@localhost:5432/example?sslmode=disable'
```

3. Run migration
```
migrate -database ${POSTGRESQL_URL} -path migrations up
```

4. View the columns created
```
psql example -c "\d stock"
psql example -c "\d sales"
```

5. To drop table
```
migrate -database ${POSTGRESQL_URL} -path migrations down
```

6. To tansfer data from csv to postgresql
```
node seeder.js
```

**URL to run this in postman**  
  
Login and create token and paste them in authentication header.

1. To run profit.js
```
http://localhost:3001/profit?tillon=till&dt=11-11-2019
```

2. To run inStock.js
```
http://localhost:3001/inStock?item=Plums&dt=20-12-2019
```

3. To run popular_items.js
```
http://localhost:3001/popular_items?desc=desc&records=3&tillon=till&dt=12-12-2019
```
4. To run profitable_items.js
```
http://localhost:3001/profitable_items?desc=desc&records=2&tillon=till&dt=12-12-2019
```
5. To run daily_profit.js
```
http://localhost:3001/daily_profit?desc=desc&records=2&dt=12-12-2019
```
6. To create record with post request
```
http://localhost:3001/create
```
7. To update record with put request
```
http://localhost:3001/update
```
8. To delete record with delete request
```
http://localhost:3001/delete?id=501
```

**Testing files from controller**  

To run all the test at once
```
npm test
```

To run individual test
```
./node_modules/.bin/mocha test/name of the file from controller.js
//example
./node_modules/.bin/mocha test/profit.js
```



**End the server in the cli running**
```
control + c
```