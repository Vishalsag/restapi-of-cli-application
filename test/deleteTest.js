var expect = require('chai').expect;
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
var request = require('request')


describe('Delete',function(){
  var idf
  it('Created dummy record',function(done){
    chai.request('http://localhost:3000') 
    .post('/create')
    .send({
            "item_name" : "fruits",
            "purchase_date" : "2019-11-11",
            "qty" : "15",
            "unit" : "kg",
            "price" : "15"
          })
    .end((err,res)=>{
      idf =res.body.id
      done()
    }) 
  });
  it('should return item created by responding 200',function(done){
    chai.request('http://localhost:3000')
      .delete('/delete')
      .query({id: idf})
      .end((err,res)=>{
        expect(res).to.have.status(200);
        done()
    })
  })
})
  




