var request = require('request')
var assert = require('chai').assert;

// Describe is used to describes the title of the test
describe('Popular_items',function(){
  //it describes what it the expected output
    it('should return Popular_items by responding code 200',function(){
        request.get({
            url:'http://localhost:3000/popular_items?desc=desc&records=1&tillon=till&dt=12-12-2019'
          }, function(error, response, body){
            //assert from chai is used to compare the expected and real output
            assert.equal(response.statusCode, 200);
          });
    })
});
