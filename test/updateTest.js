var expect = require('chai').expect;
let chai = require('chai');
let chaiHttp = require('chai-http');
//var should = require('chai').should();
chai.use(chaiHttp);
var request = require('request')


describe('Update',function(){
  var idf
  it('created dummy record',function(done){
    request.post({
        url:     'http://localhost:3000/create',
        json:{
            "item_name" : "fruits",
            "purchase_date" : "2019-11-11",
            "qty" : "15",
            "unit" : "kg",
            "price" : "15"
        }
      }, function(error, response, body){
        idf = body.id
        expect(response).to.have.status(200);
        done();
      });
  });
  it('should return updated by responding code 200',function(done){
        chai.request('http://localhost:3000')
        .put('/update')
        .send({
          "id" : idf,
          "item_name" : "Basil",
          "purchase_date" : "2019-09-01",
          "qty" : "525",
          "unit" : "kg",
          "price" : "40.29"
        })
        .end((err,res)=>{
          expect(res).to.have.status(200);
          done();
        })
    })
});
  




// // Describe is used to describes the title of the test
// describe('Update',function(){
//   //it describes what it the expected output
//     it('should return Update by responding code 200',function(){
//         request.put({
//             url:'http://localhost:3000/update',
//             json: {
//                 "id" : "561",
//                 "item_name" : "Basil",
//                 "purchase_date" : "2019-09-01",
//                 "qty" : "525",
//                 "unit" : "kg",
//                 "price" : "40.29"
//             }
//           }, function(error, response, body){
//             //assert from chai is used to compare the expected and real output
//             assert.equal(response.statusCode, 200);
//           });
//     })
// });
