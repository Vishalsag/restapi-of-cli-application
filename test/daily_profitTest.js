var request = require('request')
var assert = require('chai').assert;

// Describe is used to describes the title of the test
describe('Dailyprofit',function(){
  //it describes what it the expected output
    it('should return daily profit by responding code 200',function(){
        request.get({
            url:     'http://localhost:3000/daily_profit?desc=desc&records=3&dt=12-12-2020'
          }, function(error, response, body){
            //assert from chai is used to compare the expected and real output
            assert.equal(response.statusCode, 200);
          });
    })
});
