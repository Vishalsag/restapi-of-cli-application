//var request = require('request')
//var assert = require('chai').assert;
var expect = require('chai').expect;
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
//var request = require('request')

// Describe is used to describes the title of the test
describe('Create',function(){
  //it describes what it the expected output
    it('should return item created by responding 200',function(done){
      chai.request('http://localhost:3000') 
      .post('/create')
      .send({
              "item_name" : "fruits",
              "purchase_date" : "2019-11-11",
              "qty" : "15",
              "unit" : "kg",
              "price" : "15"
            })
      .end((err,res)=>{
        expect(res).to.have.status(200);
        done()
      })
    })
});
