var request = require('request')
var assert = require('chai').assert;

// Describe is used to describes the title of the test
describe('Profitable',function(){
  //it describes what it the expected output
    it('should return Profitable by responding code 200',function(){
        request.get({
            url:'http://localhost:3000/profitable_items?desc=desc&records=2&tillon=till&dt=12-12-2019'
          }, function(error, response, body){
            //assert from chai is used to compare the expected and real output
            assert.equal(response.statusCode, 200);
          });
    })
});
