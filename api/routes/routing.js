const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth')

const profitController = require("../controllers/profit");
const inStockController = require("../controllers/inStock");
const popularController = require("../controllers/popular_items");
const profitableController = require("../controllers/profitable_items");
const dailyController = require("../controllers/daily_profit");
const createController = require("../controllers/create");
const updateController = require("../controllers/update");
const deleteController = require("../controllers/delete");
const userController = require("../controllers/user");
const readController = require("../controllers/read");


router.get('/profit',checkAuth,profitController.profitlogic)
router.get('/inStock',checkAuth,inStockController.inStocklogic)
router.get('/popular_items',checkAuth,popularController.popularlogic)
router.get('/profitable_items',checkAuth,profitableController.profitablelogic)
router.get('/daily_profit',checkAuth,dailyController.dailylogic)
router.post('/create',checkAuth,createController.create)
router.put('/update',checkAuth,updateController.update)
router.delete('/delete',checkAuth,deleteController.delete)
router.get('/read',readController.readme)
router.post('/signup',userController.sign_up)
router.delete('/signup/:userId',userController.deleteuser)
router.post('/login',userController.login)


module.exports = router;