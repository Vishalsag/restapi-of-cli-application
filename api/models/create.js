
const pool = require('../../config/pgdb');
const moment = require('moment');
var squel = require("squel");
squelPostgres = squel.useFlavour('postgres');

pool.connect(function(err){
    if(err)
    {
        console.log(err);
    }
});

module.exports = function(item_name,purchase_date,qty,unit,price){
    return new Promise((resolve)=>{
        var sql = squelPostgres.insert()
        .into("stock")
        .setFields({ item_name:item_name,purchase_date:purchase_date,qty:qty,unit:unit,price:price })
        .returning('id')
        .toString()
        pool.query(sql, function(err,results){
            if(err)
            {
                console.log(err);
            }else{
                resolve(results.rows[0].id)
            }
        });
    })
}
