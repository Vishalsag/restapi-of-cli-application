
const pool = require('../../config/pgdb');
const moment = require('moment');
var squel = require("squel");
squelPostgres = squel.useFlavour('postgres');

pool.connect(function(err){
    if(err)
    {
        console.log(err);
    }
});

exports.signup = function(email,password){
    return new Promise((resolve,reject)=>{
        var sql = squelPostgres.insert()
        .into("users")
        .setFields({ email:email,password:password })
        .returning('user_id')
        .toString()
        pool.query(sql, function(err,results){
            if(err)
            {
                //console.log(err);
                reject(new Error(err))
            }else{
                resolve(results.rows[0].user_id)
            }
        });
    })
}

exports.login = function(email){
    var count
    return new Promise((resolve)=>{

        var sqle = squel.select()
        .from("users")
        .where('email = ?',email)
        .toString()
        pool.query(sqle, function(err,result){
            if(err)
            {
                resolve(undefined)
            }
            if(result.rows.length > 0){
                count=1
                resolve(result)
            }else{
                count=0
                resolve(undefined)
            } 
            
            resolve(result)
        });
    })
}

exports.deleteid = function(id){
    var count
    return new Promise((resolve)=>{
        var sql = squel.delete()
        .from("users")
        .where("user_id = ?",id)
        .toString()

        var sqle = squel.select()
        .from("users")
        .field('user_id')
        .where('user_id = ?',id)
        .toString()
        //console.log(sqle)
        pool.query(sqle, function(err,result){
            if(err)
            {
                console.log(err);
            }
            if(result.rows.length > 0){
                count=1
            }else count=0
        });

        pool.query(sql, function(err,results){
            if(err)
            {
                console.log(err);
            }
            resolve(count)
        });
    })
}