
var fs = require('fs');
var csv = require('csv-parser');
const pool = require('../../config/pgdb');
const moment = require('moment');
var squel = require("squel");

pool.connect(function(err){
    if(err)
    {
        console.log(err);
    }
});

module.exports = function(id,item_name,purchase_date,qty,unit,price){
    return new Promise((resolve)=>{
        var sql = squel.update()
        .table("stock")
        .set("item_name = ?", item_name)
        .set("purchase_date = ?", purchase_date)
        .set("qty = ?", qty)
        .set("unit = ?", unit)
        .set("price = ?", price)
        .where("id = ?",id)
        .toString()

        var sqle = squel.select()
        .from("stock")
        .field('id')
        .where('id = ?',id)
        .toString()

        pool.query(sqle, function(err,result){
            if(err)
            {
                console.log(err);
            }
            if(result.rows.length > 0){
                count=1
            }else count=0
        });

        pool.query(sql, function(err,results){
            if(err)
            {
                console.log(err);
            }
            resolve(count)
            
        });
    })
}
