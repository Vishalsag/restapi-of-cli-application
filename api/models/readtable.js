
var fs = require('fs');
var csv = require('csv-parser');
const pool = require('../../config/pgdb');
const moment = require('moment');
var squel = require("squel");

pool.connect(function(err){
    if(err)
    {
        console.log(err);
    }
});

module.exports = function(table){
    return new Promise((resolve)=>{
        var sql = squel.select().from(table).toString();
        pool.query(sql, function(err,results){
            if(err)
            {
                console.log(err);
            }
            var passvar = []
            for(var i=0;i<results.rows.length;i++){
                if(table === 'sales'){
                    passvar.push({
                        id:results.rows[i].id,
                        item_name:results.rows[i].item_name,
                        sales_date:moment(results.rows[i].sales_date,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format("DD-MM-YYYY"),
                        qty:results.rows[i].qty,
                        unit:results.rows[i].unit,
                        price:results.rows[i].price
                    })
                }
                else if(table === 'stock'){
                    passvar.push({
                        id:results.rows[i].id,
                        item_name:results.rows[i].item_name,
                        purchase_date:moment(results.rows[i].purchase_date,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format("DD-MM-YYYY"),
                        qty:results.rows[i].qty,
                        unit:results.rows[i].unit,
                        price:results.rows[i].price
                    })
                }else{
                    passvar.push(results.rows)
                }

            }
            resolve(passvar)
        });
    })
}
 
