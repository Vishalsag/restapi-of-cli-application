const updateproduct = require('../models/update')
const moment = require('moment');

var item_name,purchase_date,qty,unit,price;
var id;

exports.update = (req,res,next)=>{
    console.log('here')
    id = req.body.id,
    item_name = req.body.item_name,
    purchase_date= moment(req.body.purchase_date,"YYYY-MM-DD").format("DD-MM-YYYY"),
    qty=req.body.qty,
    unit=req.body.unit,
    price=req.body.price
    console.log('here')
    
    updateproduct(id,item_name,purchase_date,qty,unit,price)
    .then(result=>{
        if(result == 1)
            res.status(200).json({
                message: 'Item updated'
            })
        else{
            res.status(404).json({
                message: 'Id not found'
            })
        }
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}