//const getdata = require('../models/read');
const getdata = require('../models/read')
const moment = require('moment');
var tillon
var dt



var gettotalexpense = function(item){
    var purchase=0;
        purchase=item.reduce((total,column)=> {
            if(tillon == 'till'){
                if((moment(column.purchase_date,"DD-MM-YYYY")) <= (moment(dt,"DD-MM-YYYY"))){
                    return total + (column.qty * column.price)
                }else return total
            }else if(tillon == 'on'){
                if((moment(column.purchase_date,"DD-MM-YYYY")).isSame((moment(dt,"DD-MM-YYYY")))){
                    return total + (column.qty * column.price)
                } else return total;
            }else{
                return total + (column.qty * column.price)
            }
        },0)
        return(purchase)
}

var gettotalincome = function(item){
    var purchase=0;
        purchase=item.reduce((total,column)=> {
            if(tillon == 'till'){
                if((moment(column.sales_date,"DD-MM-YYYY")) <= (moment(dt,"DD-MM-YYYY"))){
                    return total + (column.qty * column.price)
                }else return total
            }else if(tillon == 'on'){
                if(moment(column.sales_date,"DD-MM-YYYY").isSame((moment(dt,"DD-MM-YYYY")))){
                    return total + (column.qty * column.price)
                } else return total;
            }else{
                return total + (column.qty * column.price)
            }
        },0)
        return(purchase)
}

var expense = function(){
    return new Promise((resolve,reject)=>{
            getdata('stock').then((filedata)=>{
                resolve((gettotalexpense(filedata)))
        })
    })
}

var income = function(){
    return new Promise((resolve,reject)=>{
            getdata('sales').then((filedata)=>{
                //console.log(filedata)
                resolve((gettotalincome(filedata)))
        })
    })
}

exports.profitlogic = (req,res,next)=>{
    console.log(req.headers.authorization)
    tillon = req.query.tillon
    dt = moment(req.query.dt,"YYYY-MM-DD").format("DD-MM-YYYY")
    Promise.all([income(),expense()])
    .then(result=>{
        res.status(200).json({
            NetProfit : 'Rs: ' +(result[0]-result[1]).toFixed(2)
        })
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}