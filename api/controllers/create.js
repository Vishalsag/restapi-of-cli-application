const createproduct = require('../models/create')
const moment = require('moment');

var item_name,purchase_date,qty,unit,price

exports.create = (req,res,next)=>{
    item_name = req.body.item_name,
    purchase_date = moment(req.body.purchase_date,"YYYY-MM-DD").format("DD-MM-YYYY"),
    qty=req.body.qty,
    unit=req.body.unit,
    price=req.body.price
    
    createproduct(item_name,purchase_date,qty,unit,price)
    .then(result=>{
        res.status(200).json({
            message: 'Item created',
            id: result
        })
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}