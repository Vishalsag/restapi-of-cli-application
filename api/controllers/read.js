const read = require('../models/readtable')
const moment = require('moment');

var table

exports.readme = (req,res,next)=>{
    table = req.query.table
    console.log(req.query)
    
    read(table)
    .then(result=>{
        res.status(200).json({
            Table_data: result
        })
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}