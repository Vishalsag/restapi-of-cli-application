const fs = require('fs')
const csv = require('csv-parser')
//const getdata = require('./getdata')
const getdata = require('../models/read')
const moment = require('moment');
var desc
var records
var tillon
var dt
var passvar =[]


var stockarr = function(item){
    var temp=item
    var tempe=[]
    var count =0
        //For testing purpose
        // for(let i=0;i<item.length;i++){
        //     temp.push(item[i])
        // }
        for(let i=0;i<temp.length;i++){
            if(tempe.length == 0){
                if(temp[i].unit == 'g'){
                    tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number((temp[i].qty/1000))})
                }else{
                    tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number(temp[i].qty)})
                }
            }
            else{
                count=0
                for(let j=0;j<tempe.length;j++){
                    if(temp[i].item_name == tempe[j].item_name){
                        count=1
                        tempe[j].cost = Number(tempe[j].cost) + Number(temp[i].qty*temp[i].price)                    
                        if(temp[i].unit == 'g'){
                            tempe[j].qty = Number(tempe[j].qty) + Number((temp[i].qty/1000))                        
                        }else{tempe[j].qty = Number(tempe[j].qty) + Number(temp[i].qty) 
                            }
                    }
                }
                if(count == 0){             
                    if(temp[i].unit == 'g'){
                        tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number((temp[i].qty/1000))})
                    }else{
                        tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number(temp[i].qty)})
                    }
                }
            }            
        }
        for(let i=0;i<tempe.length;i++){
            tempe[i].avg = Number(tempe[i].cost) / Number(tempe[i].qty)
        }
        return tempe
}

var salesarr = function(item){
    var temp=item
    var tempe=[]
    var count =0
        //For testing purpose
        // for(let i=0;i<item.length;i++){
        //     temp.push(item[i])
        // }
        for(let i=0;i<temp.length;i++){
            if(tempe.length == 0){
                if(temp[i].unit == 'g'){
                    tempe.push({item_name:temp[i].item_name,sold:Number(temp[i].qty*temp[i].price),qty:Number((temp[i].qty/1000))})
                }else{
                    tempe.push({item_name:temp[i].item_name,sold:Number(temp[i].qty*temp[i].price),qty:Number(temp[i].qty)})
                }
            }
            else{
                count=0
                for(let j=0;j<tempe.length;j++){
                    if(temp[i].item_name == tempe[j].item_name){
                        count=1
                        tempe[j].sold = Number(tempe[j].sold) + Number(temp[i].qty*temp[i].price)                    
                        if(temp[i].unit == 'g'){
                            tempe[j].qty = Number(tempe[j].qty) + Number((temp[i].qty/1000))                        
                        }else{tempe[j].qty = Number(tempe[j].qty) + Number(temp[i].qty) 
                            }
                    }
                }
                if(count == 0){             
                    if(temp[i].unit == 'g'){
                        tempe.push({item_name:temp[i].item_name,sold:Number(temp[i].qty*temp[i].price),qty:Number((temp[i].qty/1000))})
                    }else{
                        tempe.push({item_name:temp[i].item_name,sold:Number(temp[i].qty*temp[i].price),qty:Number(temp[i].qty)})
                    }
                }
            }            
        }
        return tempe
}

var profit = function(stockitem,salesitem){
    ans=[]
    for(let i=0;i<salesitem.length;i++){
        for(j=0;j<stockitem.length;j++){
            if(salesitem[i].item_name == stockitem[j].item_name){
                ans.push({item_name:salesitem[i].item_name,cost:'Rs.'+Number((stockitem[j].cost).toFixed(2)),purchasedqty:Number((stockitem[j].qty).toFixed(2)),sold:'Rs.'+Number((salesitem[i].sold).toFixed(2)),qty:Number((salesitem[i].qty).toFixed(2)),profit_in_Rs:Number((salesitem[i].sold-(stockitem[j].avg*salesitem[i].qty)).toFixed(2))})
            }
        }
    }
    return ans
}



async function f() {
    passvar =[]
    let stockcost = await getdata('stock').then(result =>{
        var sent=[]
        for(let i=0;i<result.length;i++){
            if(tillon == 'till'){
                if((moment(result[i].purchase_date,"DD-MM-YYYY")) < (moment(dt,"DD-MM-YYYY"))){
                    sent.push(result[i])
                }
            }else if(tillon == 'on'){
                if((moment(result[i].purchase_date,"DD-MM-YYYY")).isSame(moment(dt,"DD-MM-YYYY"))){
                    sent.push(result[i])
                }
            }else{
                sent.push(result[i])
            }
        }
        return stockarr(sent)
    })
    
    let stocksold = await getdata('sales').then(result=>{
        var sent=[]
        for(let i=0;i<result.length;i++){
            if(tillon == 'till'){
                if((moment(result[i].sales_date,"DD-MM-YYYY")) < (moment(dt,"DD-MM-YYYY"))){
                    sent.push(result[i])
                }
            }else if(tillon == 'on'){
                if((moment(result[i].sales_date,"DD-MM-YYYY")).isSame(moment(dt,"DD-MM-YYYY"))){
                    sent.push(result[i])
                }
            }else{
                sent.push(result[i])
            }
        }
        return salesarr(sent)
    })

    // let conversion = await getdata('unit_conversion.csv')
    // console.log(conversion)
    
    var finalprofit = profit(stockcost,stocksold)
    if(desc === "desc"){
        var order =finalprofit.sort(function (a, b) {
            return b.profit_in_Rs - a.profit_in_Rs;
          })
          if(records != undefined){
            for(let i=0;i<Number(records);i++){
                if(order[i]){
                    passvar.push(order[i])
                }
            }
        }
        else passvar.push(order)
    }else{
        var order =finalprofit.sort(function(a,b){
            return a.profit_in_Rs - b.profit_in_Rs;
        })
        if(records != undefined){
            for(let i=0;i<Number(records);i++){
                if(order[i]){
                    passvar.push(order[i])
                }
            }
        }
        else passvar.push(order)
    }
    return passvar
}




exports.profitablelogic = (req,res,next)=>{
    desc = req.query.desc
    records = req.query.records
    tillon = req.query.tillon
    dt = moment(req.query.dt,"YYYY-MM-DD").format("DD-MM-YYYY")
    if(records==""){records=undefined}

    f()
    .then(result=>{
        if(passvar.length>0){
            res.status(200).json({
                Message:"(Price,sold and cost are in Rs. and qty is in KG)",
                ProfitableItems: passvar
            })
        }else{
            res.status(404).json({
                Message:"No Records"
            })
        }
        
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}