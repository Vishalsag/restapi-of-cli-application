const User = require('../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
var email,password;

exports.sign_up = (req,res,next)=>{
    bcrypt.hash(req.body.password, 10,(err, hash)=>{
        if(err){
            return res.status(500).json({
                error: err
            })
        }
        else{
            email = req.body.email,
            password = hash
            User.signup(email,password)
            .then(result=>{
                console.log(result)
                res.status(201).json({
                    message:'User Created'
                })
            })
            .catch(err=>{
                res.status(500).json({
                    error: " "+err
                })
            })
        }
   })
}

exports.login =  (req,res,next)=>{
    User.login(req.body.email)
    .then(result=>{
        if(result != undefined){
            bcrypt.compare(req.body.password, result.rows[0].password, (err,ress)=>{
                if(err){
                    return res.status(401).json({
                        message: 'Authentication failed'
                    })
                }
                if(ress){
                    const token = jwt.sign({
                        email: result.rows[0].email,
                        userId: result.rows[0].user_id
                    }, 
                    'secret',
                    {
                        expiresIn:'1h'
                    })
                    return res.status(200).json({
                        message:'Authentication successful',
                        token: token
                    })
                }
                else{
                    return res.status(401).json({
                        message: 'Authentication failed'
                    })
                }
            })
            
        }else{
            return res.status(401).json({
                message: 'Authentication failed'
            })
        }
    })
    .catch(err=>{
        res.status(500).json({
            error: err
        })
    })
}

exports.deleteuser = (req,res,next)=>{
    User.deleteid(_id = req.params.userId)
    .then(result=>{
        if(result == 1){
            res.status(200).json({
                message: 'Deleted Successfully'
            })
        }else{
            res.status(404).json({
                message: 'Id not found'
            })
        }
    })
    .catch(err=>{
        res.status(500).json({
            error: err
        })
    })
}