const fs = require('fs')
const csv = require('csv-parser')
const moment = require('moment');
var desc
var records
var dt 
//const getdata = require('./getdata')
const getdata = require('../models/read')


var stockarr = function(item){
    var temp=item
    var tempe=[]
    var count =0
        //For testing purpose
        // for(let i=0;i<item.length;i++){
        //     temp.push(item[i])
        // }
        for(let i=0;i<temp.length;i++){
            if(tempe.length == 0){
                if(temp[i].unit == 'g'){
                    tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number((temp[i].qty/1000))})
                }else{
                    tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number(temp[i].qty)})
                }
            }
            else{
                count=0
                for(let j=0;j<tempe.length;j++){
                    if(temp[i].item_name == tempe[j].item_name){
                        count=1
                        tempe[j].cost = Number(tempe[j].cost) + Number(temp[i].qty*temp[i].price)
                        if(temp[i].unit == 'g'){
                            tempe[j].qty = tempe[j].qty + Number((temp[i].qty/1000))                        
                        }else{tempe[j].qty = tempe[j].qty + Number(temp[i].qty) 
                            }
                    }
                }
                if(count == 0){             
                    if(temp[i].unit == 'g'){
                        tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number((temp[i].qty/1000))})
                    }else{
                        tempe.push({item_name:temp[i].item_name,cost:Number(temp[i].qty*temp[i].price),qty:Number(temp[i].qty)})
                    }
                }
            }            
        }
        for(let i=0;i<tempe.length;i++){
            tempe[i].avg = Number(tempe[i].cost) / Number(tempe[i].qty)
        }
        return tempe
}

var profitdata = function(stockdata,salesdata){
    var ans=[]
    var count
    var temp=[]
    for(let i=0;i<salesdata.length;i++){
        for(j=0;j<stockdata.length;j++){
            if(salesdata[i].item_name == stockdata[j].item_name){
                ans.push({item_name:salesdata[i].item_name,sales_date:salesdata[i].sales_date,qty:Number(salesdata[i].qty),profit_in_Rs:(Number(salesdata[i].price*salesdata[i].qty)-(Number(stockdata[j].avg)*Number(salesdata[i].qty)))})
                
            }
        }
    }
    for(let i=0;i<ans.length;i++){
        count=0
        if(temp.length ==0){
            temp.push({sales_date:ans[i].sales_date,profit_in_Rs:Number((ans[i].profit_in_Rs).toFixed(2))})
        }else{
            for(let j=0;j<temp.length;j++){
                if(moment(temp[j].sales_date,"DD-MM-YYYY") == moment(ans[i].sales_date,"DD-MM-YYYY")){
                    count=1
                    temp[j].profit_in_Rs = Number(temp[j].profit_in_Rs) + Number(ans[i].profit_in_Rs)
                }
            }
            if(count ==0){
                temp.push({sales_date:ans[i].sales_date,profit_in_Rs:Number((ans[i].profit_in_Rs).toFixed(2))})
            }
        }
    }
    return temp
}

var passvar =[]

async function callme(){
    passvar =[]
    let stockdata = await getdata('stock').then(result =>{
        return stockarr(result)
    })

    let salesdata = await getdata('sales')
    let set = profitdata(stockdata,salesdata)
    let final=[]

    if(records != undefined){
        if(dt != undefined){
            for(let i=0;i<set.length;i++){
                //console.log('in1')
                if((moment(set[i].sales_date,"DD-MM-YYYY")) <= (moment(dt,"DD-MM-YYYY"))){
                    final.push(set[i])
                }
            }
        }else{
            //console.log('in2')
            for(let i=0;i<set.length;i++){
                final.push(set[i])
            }
        }
    }else{
        for(let i=0;i<set.length;i++){
            final.push(set[i])
        }
    }

    if(desc === "desc"){
        var order =final.sort(function (a, b) {
            return b.profit_in_Rs - a.profit_in_Rs;
        })
        if(records != undefined){
            for(let i=0;i< Number(records);i++){
                if(order[i]){
                    passvar.push(order[i])
                }
            }
        }
        else{
            passvar.push(order)
        }
    }else{
        order =final.sort(function(a,b){
            return a.profit_in_Rs - b.profit_in_Rs;
        })
        if(records != undefined){
            console.log('inside')
            for(let i=0;i<Number(records);i++){
                if(order[i]){
                    passvar.push(order[i])
                }
            }
        }
        else{
            passvar.push(order)
        }
    }
}





exports.dailylogic = (req,res,next)=>{
    desc = req.query.desc
    records = req.query.records

    if(req.query.dt == undefined || req.query.dt == ""){dt=undefined}
    else{
        console.log('here')
        dt = moment(req.query.dt,"YYYY-MM-DD").format("DD-MM-YYYY")
    }

    if(records==""){records=undefined}
    console.log(records,dt,req.query.dt)
    callme()
    .then(result=>{
        if(passvar.length>0){
            res.status(200).json({
                Message: 'The profit is in Rs.',
                DailyProfit: passvar
            })
        }else{
            res.status(404).json({
                Message: 'No records'
            })
        }
        
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}