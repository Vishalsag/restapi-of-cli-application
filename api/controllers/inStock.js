const fs = require('fs')
const csv = require('csv-parser')
const moment = require('moment');
const getdata = require('../models/read')
var item
var dt


var itemstock = function(){
    return new Promise((resolve,reject)=>{
        getdata('stock').then(result=> {
            result.forEach(x => {
                var count =0
                if(x.unit == 'g'){
                    x.qty = (Number(x.qty)/ Number(1000))
                }
            });
            resolve(result.reduce(function(stockqty,container){
            if(dt != undefined){
                if((moment(container.purchase_date,"DD-MM-YYYY")).isSameOrBefore(moment(dt,"DD-MM-YYYY"))){
                    if(container.item_name === item){return stockqty + +container.qty}else{return stockqty}
                }
                else return stockqty;
            }else{
                if(container.item_name === item){ return stockqty + +container.qty}else{return stockqty}
            }            
        },0))})
    })
}

var itemsales = function(){
    return new Promise((resolve,reject)=>{
        getdata('sales').then(result=>{ 
            result.forEach(x => {
                var count =0
                if(x.unit == 'g'){
                    x.qty = (Number(x.qty)/ Number(1000))
                }
            });
            resolve(result.reduce(function(stockqty,container){
            if(dt != undefined){
                if((moment(container.sales_date,"DD-MM-YYYY")).isSameOrBefore(moment(dt,"DD-MM-YYYY"))){
                    if(container.item_name === item){ return stockqty + +container.qty}else{return stockqty}
                }
                else return stockqty;
            }else{
                if(container.item_name === item){ return stockqty + +container.qty}else{return stockqty}
            }
            
        },0))})
       
    })
}
var ans
async function f(){

    await Promise.all([itemstock(),itemsales()]).then(result=>{
        //console.log(item,"is available with: ",Number(result[0]-result[1]).toFixed(2),"Kg quantity")  
        ans = Number(result[0]-result[1]).toFixed(2)
        
    })
    return ans
}



exports.inStocklogic = (req,res,next)=>{
    item = req.query.item
    if(req.query.dt){dt = moment(req.query.dt,"YYYY-MM-DD").format("DD-MM-YYYY")}else{dt = undefined}
    f()
    .then(result=>{
        res.status(200).json({
            Availability: item + ' is available with: ' + result + ' Kg quantity'
        })
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}