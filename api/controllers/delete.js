const deleteproduct = require('../models/delete')
var id,count;

exports.delete = (req,res,next)=>{
    id = req.query.id,
    table = req.query.table
    count =0
    deleteproduct(id,table)
    .then(result=>{
        if(result == 1){
            res.status(200).json({
                message: 'Deleted Successfully'
            })
        }else{
            res.status(404).json({
                message: 'Id not found'
            })
        }
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}