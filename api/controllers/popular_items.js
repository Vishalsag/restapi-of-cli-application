const fs = require('fs')
const csv = require('csv-parser')
//const getdata = require('./getdata')
const getdata = require('../models/read')
const moment = require('moment');
var desc
var records
var tillon
var dt


var popularitem = function(){
    var temp=[],temp1=[]
    return new Promise((resolve,reject)=>{
        getdata('sales').then(result=>{
            temp=result.reduce(function(accum,container){
                if(tillon == 'till'){
                    if((moment(container.sales_date,"DD-MM-YYYY")) <= (moment(dt,"DD-MM-YYYY"))){
                        accum[container.item_name] = ++accum[container.item_name] || 1
                        return accum
                    }else return accum
                }else if(tillon == 'on'){
                    if((moment(container.sales_date,"DD-MM-YYYY")).isSame(moment(dt,"DD-MM-YYYY"))){
                        accum[container.item_name] = ++accum[container.item_name] || 1
                        return accum
                    }else return accum
                }else{
                    accum[container.item_name] = ++accum[container.item_name] || 1
                    return accum
                }
            },[])
            var keys = Object.keys(temp)
            var values = Object.values(temp)
            for(let i=0;i<keys.length;i++){
                temp1.push({name:keys[i],popularity:values[i]})
            }
            resolve(temp1)
        })
    })
}




exports.popularlogic = (req,res,next)=>{
    desc = req.query.desc
    records = req.query.records
    tillon = req.query.tillon
    dt = moment(req.query.dt,"YYYY-MM-DD").format("DD-MM-YYYY")
    if(records==""){records=undefined}


    popularitem().then((result) =>{
        var passvar =[]
        if(desc === "desc"){
            var order =result.sort(function (a, b) {
                return b.popularity - a.popularity;
              })
            if(records != undefined){
                for(let i=0;i<Number(records);i++){
                    if(order[i]){
                        passvar.push(order[i])
                    }
                }
            }
            else passvar.push(order)
        }else{
            var order =result.sort(function(a,b){
                return a.popularity - b.popularity;
            })
            if(records != undefined){
                for(let i=0;i<Number(records);i++){
                    if(order[i]){
                        passvar.push(order[i])
                    }
                }
            }
            else passvar.push(order)
        }
        if(passvar.length>0){
            res.status(200).json({
                PopularItems: passvar
            })
        }else{
            res.status(404).json({
                Message: 'No records'
            })
        }   
            
    })
    .catch(err=>{
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}