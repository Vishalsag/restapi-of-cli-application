CREATE TABLE IF NOT EXISTS stock(
   id serial PRIMARY KEY,
   item_name VARCHAR (60) NOT NULL,
   purchase_date DATE,
   qty NUMERIC NOT NULL,
   unit VARCHAR (10) NOT NULL,
   price NUMERIC NOT NULL
);